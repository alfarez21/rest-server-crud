<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Siswa extends CI_Controller {

    
  use REST_Controller {
    REST_Controller::__construct as private __resTraitConstruct;
  }

  function __construct()
  {

    parent::__construct();
    $this->__resTraitConstruct();
    $this->load->model('siswa_model','siswa');

  }

  // Get Data
  public function index_get(){
    
    
    $key = $this->get('search');
    $id = $this->get('id');

    if($id === null ){

      if($key === null ){
        $siswa = $this->siswa->getAll();
      }
      else{
        $siswa = $this->siswa->searchData($key);
      }

    }
    else{
      $siswa = $this->siswa->getSiswa($id);
    }


    if ($siswa)
    {
      $this->response([
        'status' => true,
        'data' => $siswa
      ], 200); 
    }
    else
    {
      $this->response([
          'status' => false,
          'message' => 'No Siswa were found'
      ], 404); 
    }
    
  }

  //Delete Data
  public function index_delete(){

    $id = $this->delete('id');

    if($id === null ){

      $this->response([
        'status' => false,
        'message' => "Id Tidak Ada"
      ], 404);

    }
    else{

      if($this->siswa->hapusData($id)>0){
        $this->response([
          'status' => true,
          'message' => "$id berhasil dihapus"
        ], 200);
      }

      else{
        $this->response([
          'status' => false,
          'message' => "$id berhasil dihapus"
        ], 400);
      }

    }
  }

  // Add Data
  public function index_post(){

    $data = [
      'nama'=> $this->post('nama'),
      'idKelas'=> $this->post('kelas'),
      'idJurusan'=> $this->post('jurusan'),
    ];

    if( $this->siswa->AddData($data)>0){

      $this->response([
        'status'=> true,
        'message'=> 'Add Data Success'
      ],200);

    }
    else{

      $this->response([
        'status'=> true,
        'message'=> 'Add Data Failed'
      ],400);

    }

  }

  // Update Data
  public function index_put(){

    $id = $this->put('id');
    $data = [
      'nama'=> $this->put('nama'),
      'idKelas'=> $this->put('kelas'),
      'idJurusan'=> $this->put('jurusan'),
    ];

    if( $this->siswa->editData($id,$data)>0){

      $this->response([
        'status'=> true,
        'message'=> 'Change Data Success'
      ],200);

    }
    else{

      $this->response([
        'status'=> true,
        'message'=> 'Change Data Failed'
      ],400);

    }

  }

}
<?php

class siswa_model extends CI_Model {

  // Ambil Semua Data Siswa
  public function getAll(){

    $siswa = $this->db->select("*")->from("siswa");
    $siswa->join("kelas","siswa.IdKelas = kelas.IdKelas");
    $siswa->join("jurusan","siswa.IdJurusan = jurusan.IdJurusan");
    return $siswa->get()->result_array();

  }

  // Ambil Data Kelas
  public function getClasses()
  {

    return $this->db->get("kelas")->result_array();

  }
  
  // Ambil Data Jurusan
  public function getJurusan()
  {

    return $this->db->get("jurusan")->result_array();

  }

  // Tambah Data
  public function AddData($data){

    $this->db->insert("siswa",$data);

    return $this->db->affected_rows();
  }

  // Hapus Data
  public function hapusData($id){

    $this->db->where("idSiswa",$id)->delete("siswa");
    return $this->db->affected_rows();

  }

  // Ambil Satu Data Siswa
  public function getSiswa($id)
  {

    return $this->db->get_where("siswa",['idSiswa'=> $id])->row_array();

  }

  // Edit Data
  public function editData($id,$data){

    $this->db->where("idSiswa",$id);
    $this->db->update("siswa",$data);

    return $this->db->affected_rows();
  }

  public function searchData($key)
  {

    $siswa = $this->db->select("*")->from("siswa");
    $siswa->join("kelas","siswa.IdKelas = kelas.IdKelas");
    $siswa->join("jurusan","siswa.IdJurusan = jurusan.IdJurusan");
    $siswa->like("nama",$key);
    $siswa->or_like("kelas",$key);
    $siswa->or_like("jurusan",$key);
    return $siswa->get()->result_array();

  }
}